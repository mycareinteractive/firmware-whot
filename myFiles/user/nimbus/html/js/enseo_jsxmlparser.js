/**
 * @fileOverview HD2000 Nimbus Automated Room Control Javascript API
 * <p><b>Copyright (c) Enseo, Inc. 2009
 *
 * This material contains trade secrets and proprietary information
 * and	may not	 be used, copied, disclosed or	distributed	 except
 * pursuant to	a license from Enseo, Inc. Use of copyright notice
 * does not imply publication or disclosure.</b>
 */

////////////////////////////////////////////////////////////////////////
//
// Reference   : $Source: /cvsroot/calica/apps/rootfs_common/root/usr/bin/data/html/js/enseo_jsxmlparser.js,v $
//
// Revision	   : $Revision: 1.2 $
//
// Date		   : $Date: 2009-06-05 21:26:32 $
//
// Author(s)   : Tom Miller
//
// Description : HD2000 Author Engine XML to JavaScript PreLoad Parser
//
////////////////////////////////////////////////////////////////////////

/**
 * Namespace for the Nimbus JavaScript API
 * @name Nimbus
 */


/**
 * Gets an object for launching XML to JavaScript conversions
 * 
 * @return {Nimbus.getJSXMLParser} Instance of the Nimbus.getJSXMLParser object, 
 * or null if the object could not be created
 */

Nimbus.getJSXMLParser = function()
{
	// Create a new browser window class
	var win = new Nimbus.JSXMLParser();
	if (win != null) {
		Nimbus.NimbusObj.LogDebugMessage(
				Nimbus.LOG_MSG_TYPE_API_DEBUG, "Nimbus.getJSXMLParser");
	}
	return win;
};



/**
 * Object for creating and a JavaScript init Sequence from available theme XML
 * files in the Enseo Author Engine.
 * 
 * @class
 */

Nimbus.JSXMLParser = function()
{
    // Create a browser window object
    this.ParserObj = new EONimbusJSXMLParser();
    
    Nimbus.NimbusObj.LogDebugMessage(Nimbus.LOG_MSG_TYPE_API_DEBUG, "Nimbus.JSXMLParser constructor");
};

/**
* Rebuilds the master JS file from all XML files currently available
* 
* @return {Boolean} True if successful
* 
*/

Nimbus.JSXMLParser.prototype.RebuildAuthorInitJS = function()
{
    if (this.ParserObj == null)
    {
        return false;
    }
    return this.ParserObj.RebuildAuthorInitJS();
};


