/**
 * @fileOverview HD2000 Nimbus Automated Room Control Javascript API
 * <p><b>Copyright (c) Enseo, Inc. 2009
 *
 * This material contains trade secrets and proprietary information
 * and	may not	 be used, copied, disclosed or	distributed	 except
 * pursuant to	a license from Enseo, Inc. Use of copyright notice
 * does not imply publication or disclosure.</b>
 */

////////////////////////////////////////////////////////////////////////
//
// Reference   : $Source: /cvsroot/calica/apps/rootfs_common/root/usr/bin/data/html/js/enseo_power.js,v $
//
// Revision	   : $Revision: 1.4 $
//
// Date		   : $Date: 2009-09-29 19:56:02 $
//
// Author(s)   : Tom Miller
//
// Description : HD2000 Room Automation Power control object
//
////////////////////////////////////////////////////////////////////////

/**
 * Namespace for the Nimbus JavaScript API
 * @name Nimbus
 */


/**
 * Gets an interface for creating and controlling an Automated room control power object.
 * 
 * @return {Nimbus.Power} Instance of the Nimbus.Power interface, or null if the object could not be created
 */

Nimbus.getPower = function()
{
	// Create a new browser window class
	var win = new Nimbus.Power();
	if (win != null) {
		Nimbus.NimbusObj.LogDebugMessage(Nimbus.LOG_MSG_TYPE_API_DEBUG, "Nimbus.getPower");
	}
	return win;
};

/**
 * Interface for creating and controlling an Automated room control power object.  This onbject can be used
 * to controll In room light, shades, thermostat, etc.
 * 
 * @class
 */

Nimbus.Power = function(type, commtype, ipaddr, roomfilter)
{
    // Create a browser window object
    this.PowerObj = new EONimbusPower(type, commtype, ipaddr, roomfilter);
    this.ID = this.PowerObj.GetID();
    
    Nimbus.NimbusObj.LogDebugMessage(Nimbus.LOG_MSG_TYPE_API_DEBUG, "Nimbus.Power constructor, ID= " + this.ID);
};


/**
* Gets the Power ID.
* 
* @return {Number} ID
* 
*/

Nimbus.Power.prototype.getID = function()
{
    if (this.PowerObj == null)
    {
        return 0;
    }
    return this.ID;
};

/**
* Disconnects from a power control device
* 
* @return {Boolean} True if successful
* 
*/

Nimbus.Power.prototype.Disconnect = function()
{
    if (this.PowerObj == null)
    {
        return false;
    }
    return this.PowerObj.Disconnect();
};

/**
* Sends a Generic Power Command
* 
* @param {String}  cmd      Command to Send
*
* @return {Boolean} True if successful
* 
*/

Nimbus.Power.prototype.SendCommand = function(cmd)
{
    if (this.PowerObj == null)
    {
        return false;
    }
    return this.PowerObj.SendCommand(cmd);
};

/**
* Fetches the current power or control level for an Automated Room Power device synchronously
* 
* @param {Number}  ControlID      Control ID to fetch Power Level
*
* @return {Boolean} True if successful
* 
*/

Nimbus.Power.prototype.GetControlLevel = function(ControlID)
{
    if (this.PowerObj == null)
    {
        return false;
    }
    return this.PowerObj.GetControlLevel(ControlID);
};

/**
* Fetches the current power or control level for an Automated Room Power device asynchronously
* 
* @param {Number}  ControlID      Control ID to fetch Power Level
* @param {Number}  ComponentID    Component ID of device to activate
*
* @return {Boolean} True if successful
* 
*/

Nimbus.Power.prototype.QueryControlLevel = function(ControlID, ComponentID)
{
    if (this.PowerObj == null)
    {
        return false;
    }
    return this.PowerObj.QueryControlLevel(ControlID, ComponentID);
};

/**
* Sets the current power or control level for an Automated Room Power device
* 
* @param {Number}  ControlID      Control ID to set Power Level
* @param {Number}  level          new power level
*
* @return {Boolean} True if successful
* 
*/

Nimbus.Power.prototype.SetControlLevel = function(ControlID, level, tsecs, component)
{
    if (this.PowerObj == null)
    {
        return false;
    }
    return this.PowerObj.SetControlLevel(ControlID,level, tsecs, component);
};

/**
* Increments the current power or control level for an Automated Room Power device
* 
* @param {Number}  ControlID      Control ID to increment Power Level
*
* @return {Boolean} True if successful
* 
*/

Nimbus.Power.prototype.IncControlLevel = function(ControlID)
{
    if (this.PowerObj == null)
    {
        return false;
    }
    return this.PowerObj.IncControlLevel(ControlID);
};

/**
* Decrements the current power or control level for an Automated Room Power device
* 
* @param {Number}  ControlID      Control ID to Decrement Power Level
*
* @return {Boolean} True if successful
* 
*/

Nimbus.Power.prototype.DecControlLevel = function(ControlID)
{
    if (this.PowerObj == null)
    {
        return false;
    }
    return this.PowerObj.DecControlLevel(ControlID);
};

/**
* Activates a control for an Automated Room Power device (switch on)
* 
* @param {Number}  ControlID      Control ID of device to activate
* @param {Number}  ComponentID    Component ID of device to activate
*
* @return {Boolean} True if successful
* 
*/

Nimbus.Power.prototype.ActivateControl = function(ControlID, ComponentID)
{
    if (this.PowerObj == null)
    {
        return false;
    }
    return this.PowerObj.ActivateControl(ControlID, ComponentID);
};

/**
* Asynch state query of an Automated Room Power device (switch on)
* 
* @param {Number}  ControlID      Control ID of device to activate
* @param {Number}  ComponentID    Component ID of device to activate
*
* @return {Boolean} True if successful
* 
*/

Nimbus.Power.prototype.QueryControl = function(ControlID, ComponentID)
{
    if (this.PowerObj == null)
    {
        return false;
    }
    return this.PowerObj.QueryControl(ControlID, ComponentID);
};
