/**
 * @fileOverview HD2000 Nimbus_DCM Javascript API
 * <p><b>Copyright (c) Enseo, Inc. 2008
 *
 * This material contains trade secrets and proprietary information
 * and	may not	 be used, copied, disclosed or	distributed	 except
 * pursuant to	a license from Enseo, Inc. Use of copyright notice
 * does not imply publication or disclosure.</b>
 */

////////////////////////////////////////////////////////////////////////
//
// Reference   : $Source: /cvsroot/calica/apps/rootfs_common/root/usr/bin/data/html/js/enseo_nimbus_sptv.js,v $
//
// Revision	   : $Revision: 1.1 $
//
// Date		   : $Date: 2008-07-29 18:51:08 $
//
// Author(s)   : Larry F. Cockrum
//
// Description : HD2000 Nimbus_DCM javascript API.
//
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
//
// Global variables
//
////////////////////////////////////////////////////////////////////////


 var SMARTPORT_STATUS_SUCCESS          = 42;
 var SMARTPORT_STATUS_OPEN             = 43;
 var SMARTPORT_STATUS_CLOSED           = 44;
 var SMARTPORT_STATUS_INVALID_CMD      = 45;
 var SMARTPORT_STATUS_FAILURE          = 46;
 var SMARTPORT_STATUS_OFFLINE          = 47;
 var SMARTPORT_STATUS_CHECKSUM         = 48;
 var SMARTPORT_STATUS_SEQUENCE         = 49;
 var SMARTPORT_STATUS_BUFFER_OVERFLOW  = 50;
 var SMARTPORT_STATUS_BUFFER_LEN       = 51;
 var SMARTPORT_STATUS_INIT             = 52;
 var SMARTPORT_STATUS_TIMEOUT          = 53;
 var SMARTPORT_STATUS_BUSY             = 54;
 var SMARTPORT_STATUS_HARDWARE_FAILURE = 55;
 var SMARTPORT_STATUS_QUEUE_FULL       = 56;

 var current_state;

// States for Factory Test
var S_INITIALIZE       =  1;
var S_WAIT_POWER_ON    =  2;
var S_WAIT_START       =  3;
var S_PHASE1           =  4;
var S_PHASE2		   =  5;
var S_CHUPDOWN1		   =  6;
var S_CHUPDOWN2		   =  7;
var S_ONOFF1		   =  8;
var S_ONOFF2		   =  9;
var S_ONOFF3		   = 10;
var S_SAP1			   = 11;
var S_SAP2			   = 12;
var S_CH126_1		   = 13;
var S_CH126_2		   = 14;
var S_CH127_1		   = 15;
var S_CH127_2		   = 16;

// current states of DCM
var SMARTPORT_DCM_OFFLINE = "0";
var SMARTPORT_DCM_INIT_1  = "1";
var	SMARTPORT_DCM_INIT_2  = "2";
var SMARTPORT_DCM_ONLINE  = "3";
var SMARTPORT_DCM_UNKNOWN = "4";

// SP channel types
var	TV				= 0x0;
var	INFO			= 0x1;
var	PAYTV			= 0x2;
var	RADIO			= 0x3;
var	AV1				= 0x4;
var	SVIDEO			= 0x5;
var	AV2				= 0x6;
var	FRONT_SVIDEO	= 0x7;
var	HDTV			= 0x8;
var	DVD				= 0x9;
var	VGA				= 0xa;
var	HDMI			= 0xb;
var	DIGITAL			= 0xc;
var	RESERVED_d		= 0xd;
var	RESERVED_e		= 0xe;
var	RESERVED_f		= 0xf;


////////////////////////////////////////////////////////////////////////
//
// Misc functionality
//
////////////////////////////////////////////////////////////////////////


/**
 * Sends SP TV command to UUT.
 *
 * @param {String} DCM message with each field delimited by ":".
 *
 * @return None
 */
 Nimbus.SPTV_SendCommand = function(o_req) {
	 s = o_req.join("!");
	 var res;
     res = Nimbus.NimbusObj.SPTVSendCommand(s);
	 return res;
 }
 
 /**
 * Sends UUT SP TV open comms channel to the TV.
 *
 * @param None
 *
 * @return None
 */
 Nimbus.SPTVOpen = function(msg) {
     Nimbus.NimbusObj.SPTVOpen(msg);
 }		
 
 /**
 * Sends UUT SP TV close comms channel to the TV.
 *
 * @param None
 *
 * @return None
 */
 Nimbus.SPTVClose = function(msg) {
     Nimbus.NimbusObj.SPTVClose(msg);
 }		

/**
 * Gets Smart Port TV status.
 * 
 * @return {objecct}  SP TV Status. 
 */

Nimbus.GetSPTVStatus = function(){
	var temp;

	var astatus = new Array();
	temp = Nimbus.NimbusObj.GetSPTVStatus();
	astatus = temp.split("!");
	astatus[8] = astatus[8] - 0;

	var tvstatus = {tvstate:astatus[0], cmd_state:astatus[1], command:astatus[2], cmd_status:astatus[3], cmd_failed:astatus[4], S00_B1:astatus[5], S00_B2:astatus[6],
				   S01_B1:astatus[7], S01_B2:astatus[8], S71_B1:astatus[9],	S71_B2:astatus[10], S7f_B1:astatus[11], S7f_B2:astatus[12]};

	tvstatus.bTVOnline = false;
	tvstatus.bTVOn = false;

	switch(tvstatus.tvstate) {
		case SMARTPORT_DCM_OFFLINE:	tvstatus.tvstate = "OFFLINE  "; break;
		case SMARTPORT_DCM_INIT_1:	tvstatus.tvstate = "INIT_1   "; break;
		case SMARTPORT_DCM_INIT_2:	tvstatus.tvstate = "INIT_2   "; break;
		case SMARTPORT_DCM_ONLINE:	tvstatus.tvstate = "ONLINE   ";
			tvstatus.bTVOnline = true;						   
			if(tvstatus.S01_B1 & 0x80)	tvstatus.bTVOn = true; 
			break;
		default:
		     SMARTPORT_DCM_UNKNOWN:	tvstatus.tvstate = "UNKNOWN  "; break;
	}
	if (tvstatus.S00_B1 & 0x80) {
		tvstatus.tvinfo = "TV INFO: ATSC/NTSC ";
	}
	else {
		tvstatus.tvinfo = "TV INFO:      NTSC ";
	}

	tvstatus.tvmem = "Mem: " + ((tvstatus.S00_B2 & 0x0f).toString(16)) + " "; // hex
					
	tvstatus.tvchn_num = (tvstatus.S01_B1 & 0x7f) + " ";

	switch(tvstatus.S71_B1 & 0x0f) {
		case TV:			tvstatus.tvchn_typ = "(TV)"; break;
		case INFO:			tvstatus.tvchn_typ = "(INFO)          "; break;
		case PAYTV:			tvstatus.tvchn_typ = "(PAYTV)         "; break;
		case RADIO:			tvstatus.tvchn_typ = "(RADIO)         "; break;
		case AV1:			tvstatus.tvchn_typ = "(AV1)           "; break;
		case SVIDEO:		tvstatus.tvchn_typ = "(SVIDEO)        "; break;
		case AV2:			tvstatus.tvchn_typ = "(AV2)           "; break;
		case FRONT_SVIDEO:	tvstatus.tvchn_typ = "(FRONT_SVIDEO)  "; break;
		case HDTV:			tvstatus.tvchn_typ = "(HDTV)          "; break;
		case DVD:			tvstatus.tvchn_typ = "(DVD)           "; break;
		case VGA:			tvstatus.tvchn_typ = "(VGA)           "; break;
		case HDMI:			tvstatus.tvchn_typ = "(HDMI)          "; break;
		case DIGITAL:		tvstatus.tvchn_typ = "(DIGITAL)       "; break;
		case RESERVED_d:	tvstatus.tvchn_typ = "(RESERVED-D)    "; break;
		case RESERVED_e:	tvstatus.tvchn_typ = "(RESERVED-E)    "; break;
		case RESERVED_f:	tvstatus.tvchn_typ = "(RESERVED-F)    "; break;
    }
	if ( (tvstatus.S01_B2 & 0x10)) tvstatus.osd_busy = true; // OSD busy true set red
	else tvstatus.osd_busy = false;

	if (tvstatus.S01_B1 & 0x80)	tvstatus.power_on = true; //Power On
	else tvstatus.power_on = false; // Power off/ standby

	if ( tvstatus.S01_B2 & 0x40) {
		tvstatus.tvsync = "SYNC  ";
	}
	else {
	    tvstatus.tvsync = "NOVID ";
	}

	if (tvstatus.S7f_B2) tvstatus.tvrgn = "RGN:" + (tvstatus.S7f_B2 & 0xf0) >> 4 + " MEM:" + ((tvstatus.S7f_B2)& 0x0f) + " ";
	else tvstatus.tvrgn = "RGN: N/A";
	return tvstatus;
}

