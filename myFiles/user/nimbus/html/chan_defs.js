//////////////////////////////////////////////////////////////////////////////////////////
//	Enseo Nimbus API usage example
//	Channel definitions
//	Copyright (c) Enseo, Inc. 1997-2006
//////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////
// Code for building the channel list used by the Nimbus example application
//////////////////////////////////////////////////////////////////////////////////////////

// Array of available channels
var Chan_Table = new Array();
	
/*function BuildChanRF(type, freq, program, chn, label, audio_video) {
    var chanobj = new Object;
	chanobj.type = "rf";
	chanobj.ID = chn;			          	//channel number
	chanobj.program = program;	          	//program	number
	chanobj.freq = freq;			      	//frequency
	chanobj.label = label;				  	//label
	chanobj.audio_video = audio_video;	  	//A/V usage type
    return chanobj;																						
}
*/
function BuildChanRF(freq, program, chn, label, audio_video) {
    var chanobj = new Object;
	chanobj.type = "rf";
	chanobj.ID = chn;			          	//channel number
	chanobj.program = program;	          	//program	number
	chanobj.freq = freq;			      	//frequency
	chanobj.label = label;				  	//label
	chanobj.audio_video = audio_video;	  	//A/V usage type
    return chanobj;																						
}

function BuildChanIP(ip, port, chn, label, audio_video) {
    var chanobj = new Object;
	chanobj.type = "ip";
	chanobj.ID = chn;			          	//channel number
	chanobj.ip = ip;						//IP address
	chanobj.port = port						//port
	chanobj.label = label;				  	//label
	chanobj.audio_video = audio_video;	  	//A/V usage type
    return chanobj;																						
}

var idx = 0;
// IP Example:                     IP                  port        ID      label       usage: A = audio, AV = audio and video
//Chan_Table[idx++] = BuildChanIP("239.79.200.34",  "20034",    "02",   "Travel Channel HD",        "AV");
// RF Example:                    freq	   prog   ID	label  usage: A = audio,  AV = audio and video
//Chan_Table[idx++] = BuildChanRF( "657000", "1", "02", "CNN", "AV"); 
// Analog Example:                freq     nothing   ID    label  usage: A = audio,  AV = audio and video
//Chan_Table[idx++] = BuildChanRF( "657000", "", "02", "CNN", "AV");

Chan_Table[idx++] = BuildChanRF("621000", "1", "02", "Power Up", "AV");
Chan_Table[idx++] = BuildChanRF("621000", "2", "03", "Relaxation", "AV");
Chan_Table[idx++] = BuildChanRF("69000", "", "04", "CBS", "AV");
Chan_Table[idx++] = BuildChanRF("85000", "", "06", "NBC", "AV");
Chan_Table[idx++] = BuildChanRF("177000", "", "07", "PBS", "AV");
Chan_Table[idx++] = BuildChanRF("195000", "", "10", "ABC", "AV");
Chan_Table[idx++] = BuildChanRF("207000", "", "12", "UPN", "AV");
Chan_Table[idx++] = BuildChanRF("213000", "", "13", "Newborn", "AV");
Chan_Table[idx++] = BuildChanRF("123000", "", "14", "Newborn (SP)", "AV");
Chan_Table[idx++] = BuildChanRF("135000", "", "16", "FX", "AV");
Chan_Table[idx++] = BuildChanRF("147000", "", "18", "ESPN", "AV");
Chan_Table[idx++] = BuildChanRF("159000", "", "20", "WB", "AV");
Chan_Table[idx++] = BuildChanRF("165000", "", "21", "FOX", "AV");
Chan_Table[idx++] = BuildChanRF("171000", "", "22", "Lifetime", "AV");
Chan_Table[idx++] = BuildChanRF("219000", "", "23", "Lifetime Movie", "AV");
Chan_Table[idx++] = BuildChanRF("225000", "", "24", "TBS", "AV");
Chan_Table[idx++] = BuildChanRF("231000", "", "25", "USA", "AV");
Chan_Table[idx++] = BuildChanRF("237000", "", "26", "TNT", "AV");
Chan_Table[idx++] = BuildChanRF("255000", "", "29", "Discovery", "AV");
Chan_Table[idx++] = BuildChanRF("261000", "", "30", "KNWS", "AV");
Chan_Table[idx++] = BuildChanRF("267000", "", "31", "Telemundo", "AV");
Chan_Table[idx++] = BuildChanRF("273000", "", "32", "Univision", "AV");
Chan_Table[idx++] = BuildChanRF("279000", "", "33", "Cartoon Network", "AV");
Chan_Table[idx++] = BuildChanRF("285000", "", "34", "TLC", "AV");
Chan_Table[idx++] = BuildChanRF("291000", "", "35", "AMC", "AV");
Chan_Table[idx++] = BuildChanRF("297000", "", "36", "Animal Planet", "AV");
Chan_Table[idx++] = BuildChanRF("303000", "", "37", "CNN", "AV");
Chan_Table[idx++] = BuildChanRF("309000", "", "38", "A & E", "AV");
Chan_Table[idx++] = BuildChanRF("315000", "", "39", "Biography", "AV");
Chan_Table[idx++] = BuildChanRF("321000", "", "40", "Bravo", "AV");
Chan_Table[idx++] = BuildChanRF("327000", "", "41", "BET", "AV");
Chan_Table[idx++] = BuildChanRF("333000", "", "42", "ABC Family", "AV");
Chan_Table[idx++] = BuildChanRF("339000", "", "43", "CBS Sports", "AV");
Chan_Table[idx++] = BuildChanRF("345000", "", "44", "Cooking", "AV");
Chan_Table[idx++] = BuildChanRF("357000", "", "46", "CSPAN", "AV");
Chan_Table[idx++] = BuildChanRF("363000", "", "47", "E! Entertainment", "AV");
Chan_Table[idx++] = BuildChanRF("369000", "", "48", "Fox Movie", "AV");
Chan_Table[idx++] = BuildChanRF("375000", "", "49", "CNBC", "AV");
Chan_Table[idx++] = BuildChanRF("381000", "", "50", "Home & Garden", "AV");
Chan_Table[idx++] = BuildChanRF("387000", "", "51", "Food", "AV");
Chan_Table[idx++] = BuildChanRF("393000", "", "52", "Travel", "AV");
Chan_Table[idx++] = BuildChanRF("399000", "", "53", "Golf", "AV");
Chan_Table[idx++] = BuildChanRF("405000", "", "54", "ESPN2", "AV");
Chan_Table[idx++] = BuildChanRF("411000", "", "55", "MSNBC", "AV");
Chan_Table[idx++] = BuildChanRF("417000", "", "56", "National Geographic", "AV");
Chan_Table[idx++] = BuildChanRF("423000", "", "57", "Fox News", "AV");
Chan_Table[idx++] = BuildChanRF("429000", "", "58", "Nickelodeon", "AV");
Chan_Table[idx++] = BuildChanRF("435000", "", "59", "Oprah Winfrey", "AV");
Chan_Table[idx++] = BuildChanRF("441000", "", "60", "TruTV", "AV");


///////////////////////////////////////////////////////////////////////////////////////////////////
var MaxChans = Chan_Table.length;
var DefChannel = 0;

// Array of available channels30
var ChanList = new Array();

// Build a channel list object
function BuildChannel(Label, ChanDesc) {
	var ChanObj = new Object();
	ChanObj.ChanDesc = ChanDesc;
	ChanObj.Label = Label;
	return ChanObj;
}

// Build Chan_Table list
for (i=0; i < MaxChans; i++) {
    var ch = Chan_Table[i];
	var ChanUsage = "AudioVideo";
	if (ch.audio_video == "A") {
		ChanUsage = "AudioOnly";
	}
	var isDigital = true;
	var prgNum = parseInt(ch.program);
	if (isNaN(prgNum) || prgNum <= 0) {
	    isDigital = false;
	}
	
	if (ch.type == "rf" && isDigital) {
		ChanList.push(BuildChannel('"' + ch.label + '"', 
										'<ChannelParams ChannelType="Digital" \
											ChannelUsage="' + ChanUsage + '"> \
											<DigitalChannelParams \
												PhysicalChannelIDType="Freq" \
												PhysicalChannelID="' + ch.freq + '" \
												DemodMode="QAMAuto" \
												ProgramSelectionMode="PATProgram" \
												ProgramID="' + ch.program + '"> \
											</DigitalChannelParams> \
										</ChannelParams>'));
	} 
	else if (ch.type == "rf" && !isDigital) {
	    ChanList.push(BuildChannel('"' + ch.label + '"', 
                                        '<ChannelParams ChannelType="Analog" \
                                            ChannelUsage="' + ChanUsage + '"> \
                                            <AnalogChannelParams \
                                                PhysicalChannelIDType="Freq" \
                                                PhysicalChannelID="' + ch.freq + '"> \
                                            </AnalogChannelParams> \
                                        </ChannelParams>'));
	}
	else if (ch.type == "ip") {
		ChanList.push(BuildChannel('"' + ch.label + '"', 
										'<ChannelParams ChannelType="UDP" \
											Encryption="Proidiom" \
											ChannelUsage="' + ChanUsage + '"> \
											<UDPChannelParams \
												Address="' + ch.ip + '" \
												Port="' + ch.port + '"> \
											</UDPChannelParams> \
										</ChannelParams>'));
	}													
	Nimbus.logMessage("ChanList : " + ChanList[i].ChanDesc + "   " +ChanList[i].Label);
}

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

